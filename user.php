<?php 
namespace User;

use Koneksi\Koneksi;
use PDOException;
class User {
    public function __construct()
    {
       
    }

    public function login($username,$password) 
    {
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelompok=$db->prepare("SELECT * FROM user");
        $kelompok->execute();
        $hasil=$kelompok->fetchAll();

        $dbusername=$hasil[0]['username'];
        $dbpassword=$hasil[0]['password'];
        if(($username==$dbusername)&&($password==$dbpassword)){
           header("location:index.php");
        }else{
           header("location:login.php");
        }        
    }
    public function showusername($username) 
    {
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelompok=$db->prepare("SELECT * FROM user");
        $kelompok->execute();
        $hasil=$kelompok->fetchAll();

        $dbusername=$hasil[0]['username'];
        if(($username==$dbusername)){
           header("location:index.php");
        }else{
           header("location:login.php");
        }        
    }

    public function showDataKelasSma($kelas_sma)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $semuakelas="SELECT NIS,nama,kelas,Tingkatan,tgl_lahir,jk,alamat, nama_jurusan FROM siswa where (kelas='X' OR kelas='XI' OR kelas='XII') ORDER BY nama ASC";
        $kelas= "AND kelas='".$kelas_sma."'";
        $kelasx="";
        if($kelas_sma != ''){
            $kelasx = $semuakelas . $kelas;
        }else{
            $kelasx = $semuakelas;
        }
		$stmt=$db->prepare($kelas_sma);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataJurusanSma($nama_jurusan,$kelas)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelas_sma="SELECT * FROM siswa where (kelas='X' OR kelas='XI' OR kelas='XII')";
        $IPA= "AND (nama_jurusan='IPA' OR nama_jurusan='IPS')";
        $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
        $kelasx= "AND kelas='".$kelas."'";

        $jurusankelas="";
       
        if($nama_jurusan != ''){
           $jurusankelas = $kelas_sma . $jurusan; 
          
        }if($kelas != ''){
            $jurusankelas = $jurusankelas . $kelasx ;
        }elseif($kelas != '') {
            $jurusankelas = $kelas_sma . $IPA;
             
        }elseif($nama_jurusan != '') {
            $jurusankelas = $jurusankelas ;
        }else{
           $jurusankelas = $kelas_sma ;
        }
		$stmt=$db->prepare($jurusankelas);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataJurusanSma1($nama_jurusan,$kelas,$halaman_awal,$batas)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelas_sma="SELECT * FROM siswa where (kelas='X' OR kelas='XI' OR kelas='XII')";
        $IPA= "AND (nama_jurusan='IPA' OR nama_jurusan='IPS')";
        $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
        $kelasx= "AND kelas='".$kelas."'";

        $jurusankelas="";
       
        if($nama_jurusan != ''){
           $jurusankelas = $kelas_sma . $jurusan; 
          
        }if($kelas != ''){
            $jurusankelas = $jurusankelas . $kelasx ;
        }elseif($kelas != '') {
            $jurusankelas = $kelas_sma . $IPA;
             
        }elseif($nama_jurusan != '') {
            $jurusankelas = $jurusankelas ;
        }else{
           $jurusankelas = $kelas_sma ;
        }
		$stmt=$db->prepare($jurusankelas . " LIMIT $halaman_awal , $batas");
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataGuruJurusanSma($nama_jurusan,$kelas)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelas_sma="SELECT * FROM guru where (kelas='X' OR kelas='XI' OR kelas='XII')";
        $IPA= "AND (nama_jurusan='IPA' OR nama_jurusan='IPS')";
        $NIG1 = "AND ( NIG='2147483647' OR NIG='17236457' OR NIG='172345765' OR NIG='1267853' OR NIG='1231241' OR NIG='	1231241212' OR NIG='66666')";
        $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
        $kelasx= "AND kelas='".$kelas."'";

        $jurusankelas="";
       
        if($nama_jurusan != ''){
           $jurusankelas = $kelas_sma . $jurusan . $NIG1; 
          
        }if($kelas != ''){
            $jurusankelas = $jurusankelas . $kelasx . $NIG1;
        }elseif($kelas != '') {
            $jurusankelas = $kelas_sma . $IPA;
             
        }elseif($nama_jurusan != '') {
            $jurusankelas = $jurusankelas ;
        }else{
           $jurusankelas = $kelas_sma . $NIG1;
        }
		$stmt=$db->prepare($jurusankelas);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataMapelGuru(){
        $conn = new Koneksi();
        $db=$conn->metal();
        $mapel= "SELECT * FROM mapel ";
		$stmt=$db->prepare($mapel);
		$stmt->execute(); 
		return $stmt;
    }
    public function showDataMapelGuru1($halaman_awal,$batas){
        $conn = new Koneksi();
        $db=$conn->metal();
        $mapel= "SELECT * FROM mapel ";
		$stmt=$db->prepare($mapel." LIMIT $halaman_awal , $batas");
		$stmt->execute(); 
		return $stmt;
    }
    public function showDataSmp($kelas_smp)
	{
        
        $conn = new Koneksi();
        $db=$conn->metal();
        $semuakelas= "SELECT * FROM siswa where (kelas='VII' OR kelas='VIII' OR kelas='IX')" ;
        $kelasvii="AND kelas='".$kelas_smp."'";
        $kelas="";
        if($kelas_smp != ''){
            $kelas = $semuakelas . $kelasvii;
        }else{
            $kelas = $semuakelas;
        }
		$stmt=$db->prepare($kelas);
		$stmt->execute(); 
		return $stmt;
        }
        public function showDataSmp1($kelas_smp,$halaman_awal,$batas)
        {
            $conn = new Koneksi();
            $db=$conn->metal();
            $semuakelas= "SELECT * FROM siswa where (kelas='VII' OR kelas='VIII' OR kelas='IX') " ;
            $kelasvii="AND kelas='".$kelas_smp."'";
            $kelas="";
            if($kelas_smp != ''){
                $kelas = $semuakelas . $kelasvii ;
            }else{
                $kelas = $semuakelas;
            }
            
            $stmt=$db->prepare($kelas ." LIMIT $batas,$halaman_awal");
            $stmt->execute(); 
            return $stmt;
            }
        

    public function showDataKelasSmp()
	{
        $conn = new Koneksi();
        $db=$conn->metal();
		$stmt=$db->prepare("SELECT kelas FROM kelas where tingkatan='SMP'");
		$stmt->execute(); 
		return $stmt;
	}
    
    public function showDatavii()
	{
        $conn = new Koneksi();
        $db=$conn->metal();
		$stmt=$db->prepare("SELECT NIS,nama,kelas,tgl_lahir,jk,alamat,nama FROM siswa where kelas='VII' ORDER BY nama ASC");
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataNamaKelas($kelas_smp)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $semuakelas= "SELECT * FROM guru where (kelas='VII' OR kelas='VIII' OR kelas='IX' )";
        $kelasvii="AND kelas='".$kelas_smp."'";
        $NIG1 = "AND (NIG='323456' OR NIG='325647' OR NIG='321435' )";
        $kelas="";
        if($kelas_smp != ''){
            $kelas = $semuakelas . $kelasvii . $NIG1;
        }elseif($kelas_smp != ''){
            $kelas = $semuakelas . $kelasvii ;
        }else{
            $kelas = $semuakelas . $NIG1;
            
        }
		$stmt=$db->prepare($kelas);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataGuruvii($kelas_smp)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $semuakelas= "SELECT * FROM guru where (kelas='VII' OR kelas='VIII' OR kelas='IX' )";
        $kelasvii="AND kelas='".$kelas_smp."'";
        $NIG1 = "AND (NIG='12412341' OR NIG='123124124' OR NIG='12847912' )";
        $kelas="";
        if($kelas_smp != ''){
            $kelas = $semuakelas . $kelasvii . $NIG1;
        }elseif($kelas_smp != ''){
            $kelas = $semuakelas . $kelasvii ;
           
        }else{
            $kelas = $semuakelas . $NIG1;
            
        }
		$stmt=$db->prepare($kelas);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataGuru($nama_jurusan,$kelas)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelas_sma="SELECT * FROM guru where (kelas='X' OR kelas='XI' OR kelas='XII')";
        $IPA= "AND (nama_jurusan='IPA' OR nama_jurusan='IPS')";;
        $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
        $kelasx= "AND kelas='".$kelas."'";

        $jurusankelas="";
       
        if($nama_jurusan != ''){
           $jurusankelas = $kelas_sma . $jurusan ; 
          
        }if($kelas != ''){
            $jurusankelas = $jurusankelas . $kelasx ;
        }elseif($kelas != '') {
            $jurusankelas = $kelas_sma . $IPA;
             
        }elseif($nama_jurusan != '') {
            $jurusankelas = $jurusankelas ;
        }else{
           $jurusankelas = $kelas_sma;
        }
		$stmt=$db->prepare($jurusankelas);
		$stmt->execute(); 
		return $stmt;
	}
    public function showDataGuru1($nama_jurusan,$kelas,$halaman_awal,$batas)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $kelas_sma="SELECT * FROM guru where (kelas='VII' OR kelas='VIII' OR kelas='IX' OR kelas='X' OR kelas='XI' OR kelas='XII')";
        $IPA= "AND (nama_jurusan='TIDAK ADA' OR nama_jurusan='IPA' OR nama_jurusan='IPS')";;
        $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
        $kelasx= "AND kelas='".$kelas."'";

        $jurusankelas="";
       
        if($nama_jurusan != ''){
           $jurusankelas = $kelas_sma . $jurusan ; 
          
        }if($kelas != ''){
            $jurusankelas = $jurusankelas . $kelasx ;
        }elseif($kelas != '') {
            $jurusankelas = $kelas_sma . $IPA;
             
        }elseif($nama_jurusan != '') {
            $jurusankelas = $jurusankelas ;
        }else{
           $jurusankelas = $kelas_sma ;
        }
		$stmt=$db->prepare($jurusankelas . " LIMIT $halaman_awal,$batas");
		$stmt->execute(); 
		return $stmt;
	}
    public function insertDataGuru($NIG,$nama,$tgl_lahir,$jk,$alamat,$kelas,$Tingkatan,$nama_jurusan,$nama_mapel)
	{
        if(isset($_POST['input']))
		{
            
                $conn = new Koneksi();
                $db=$conn->metal();
                $sql="INSERT INTO guru (NIG,nama,tgl_lahir,jk,alamat,kelas,Tingkatan,nama_jurusan,mapel) VALUES (:NIG,:nama,:tgl_lahir,:jk,:alamat,:kelas,:Tingkatan,:nama_jurusan,:nama_mapel)";
                $stmt=$db->prepare($sql);
				$stmt->bindParam(":NIG", $NIG);
				$stmt->bindParam(":nama", $nama);
                $stmt->bindParam(":kelas", $kelas);
                $stmt->bindParam(":Tingkatan", $Tingkatan);
                $stmt->bindParam(":tgl_lahir", $tgl_lahir);
                $stmt->bindParam(":jk", $jk);
                $stmt->bindParam(":alamat", $alamat);
                $stmt->bindParam(":nama_jurusan",$nama_jurusan);
                $stmt->bindParam(":nama_mapel", $nama_mapel);             
				
                if (!$stmt->execute()) {
                    echo "\nPDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                    die();
                    
                }
				return true;
                header("location:guru.php");
		}
        
    }
    
    public function deleteGuru($NIG)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $sql ="DELETE FROM guru WHERE NIG = :NIG";
        $stmt =$db->prepare($sql);
         $stmt->bindParam(":NIG", $NIG);
         $stmt->execute();
        return true;
        header("location:guru.php");
	}
    public function editGuru($NIG,$nama,$kelas,$Tingkatan,$tgl_lahir,$jk,$alamat,$nama_jurusan)
    {
        try{
            $conn = new Koneksi();
            $db=$conn->metal();
            $query = "UPDATE guru SET NIG=:NIG, nama=:nama, kelas=:kelas, Tingkatan=:Tingkatan, tgl_lahir=:tgl_lahir, jk=:jk, alamat=:alamat, nama_jurusan=:nama_jurusan WHERE NIG=:NIG";
            $statement = $db->prepare($query);
            $data = [
                ':NIG' => $NIG,
                ':nama' => $nama,
                ':kelas' => $kelas,
                ':Tingkatan' => $Tingkatan,
                ':tgl_lahir' => $tgl_lahir,
                ':jk' => $jk,
                ':alamat' => $alamat,
                ':nama_jurusan' => $nama_jurusan
            ];
            $query_execute = $statement->execute($data);
            if($query_execute)
            {
                header('Location:guru.php');
                exit(0);
            }
            else
            {
                header('Location:guru.php?NIG=' . $NIG);
                exit(0);
            }
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function insertDataSiswaSma($NIS,$nama,$kelas,$Tingkatan,$tgl_lahir,$jk,$alamat,$nama_jurusan)
	{
        if(isset($_POST['input']))
        
		{
           
           
                $conn = new Koneksi();
                $db=$conn->metal();
                $sql="INSERT INTO siswa (NIS,nama,kelas,Tingkatan,tgl_lahir,jk,alamat,nama_jurusan) VALUES (:NIS,:nama,:kelas,:Tingkatan,:tgl_lahir,:jk,:alamat,:nama_jurusan)";
                $stmt=$db->prepare($sql);
				$stmt->bindParam(":NIS", $NIS);
				$stmt->bindParam(":nama", $nama);
                $stmt->bindParam(":kelas", $kelas);
                $stmt->bindParam(":Tingkatan", $Tingkatan);
                $stmt->bindParam(":tgl_lahir", $tgl_lahir);
                $stmt->bindParam(":jk", $jk);
                $stmt->bindParam(":alamat", $alamat);
                $stmt->bindParam(":nama_jurusan", $nama_jurusan);

				//$stmt->execute();
                $stmt->execute();
                 
                }
				return true;
                header("location:sma.php");
		
        
    }
    public function deletesma($NIS)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $sql ="DELETE FROM siswa WHERE NIS = :NIS";
        $stmt =$db->prepare($sql);
         $stmt->bindParam(":NIS", $NIS);
         $stmt->execute();
        return true;
        header("location:sma.php");
	}
    public function editSiswasma($NIS,$nama,$kelas,$tgl_lahir,$jk,$alamat,$nama_jurusan)
    {
       try {
            $conn = new Koneksi();
            $db=$conn->metal();
            $query = "UPDATE Siswa SET NIS=:NIS, nama=:nama, kelas=:kelas, tgl_lahir=:tgl_lahir, jk=:jk, alamat=:alamat, nama_jurusan=:nama_jurusan WHERE NIS=:NIS";
            $statement = $db->prepare($query);
            $data = [
                ':NIS' => $NIS,
                ':nama' => $nama,
                ':kelas' => $kelas,
                ':tgl_lahir' => $tgl_lahir,
                ':jk' => $jk,
                ':alamat' => $alamat,
                ':nama_jurusan' => $nama_jurusan
            ];
            $query_execute = $statement->execute($data);
            if($query_execute)
            {
                header('Location:sma.php');
                exit(0);
            }
            else
            {
                header('Location:sma.php?NIS=' . $NIS );
                exit(0);
            }
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function deletesmp($NIS)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $sql ="DELETE FROM siswa WHERE NIS = :NIS";
        $stmt =$db->prepare($sql);
         $stmt->bindParam(":NIS", $NIS);
         $stmt->execute();
        return true;
        header("location:smp.php");
	}
    public function editSiswasmp($NIS,$nama,$kelas,$tgl_lahir,$jk,$alamat)
    {
            try {
                $conn = new Koneksi();
                $db=$conn->metal();
                $query = "UPDATE siswa SET NIS=:NIS, nama=:nama, kelas=:kelas, tgl_lahir=:tgl_lahir, jk=:jk, alamat=:alamat WHERE NIS=:NIS";
                $statement = $db->prepare($query);
                $data = [
                    ':NIS' => $NIS,
                    ':nama' => $nama,
                    ':kelas' => $kelas,
                    ':tgl_lahir' => $tgl_lahir,
                    ':jk' => $jk,
                    ':alamat' => $alamat
                ];
                $query_execute = $statement->execute($data);
                if($query_execute)
                {
                    header('Location:smp.php');
                    exit(0);
                }
                else
                {
                    header('Location:editsmp.php?NIS=' . $NIS );
                    exit(0);
                }
            }catch (PDOException $e) {
                echo $e->getMessage();
            }
        
    }

    public function showDataMapel(){
        $conn = new Koneksi();
        $db=$conn->metal();
		$stmt=$db->prepare("SELECT * FROM mapel");
		$stmt->execute(); 
		return $stmt;
    }

    public function insertDataMapel($mapel)
	{
        if(isset($_POST['input']))
		{
                $conn = new Koneksi();
                $db=$conn->metal();
                $sql="INSERT INTO mapel (nama_mapel) VALUES (:nama_mapel)";
                $stmt=$db->prepare($sql);
				$stmt->bindParam(":nama_mapel", $mapel);            
				$stmt->execute();
				return true;
                header("location:datamapel.php");
		}
        
    }

    public function deleteMapel($mapel)
	{
        $conn = new Koneksi();
        $db=$conn->metal();
        $sql ="DELETE FROM mapel WHERE nama_mapel = :nama_mapel";
        $stmt =$db->prepare($sql);
         $stmt->bindParam(":nama_mapel", $mapel);
         $stmt->execute();
        return true;
        header("location:datamapel.php");
	}

    public function editMapel($nama_mapel,$id_mapel)
    {
     try {
            $conn = new Koneksi();
            $db=$conn->metal();
            $query = "UPDATE mapel SET nama_mapel=:nama_mapel WHERE id_mapel = :id_mapel";
            $statement = $db->prepare($query);
            $data = [
                ':nama_mapel' => $nama_mapel,
                ':id_mapel' => $id_mapel
            ];
            $query_execute = $statement->execute($data);
      
            
            if($query_execute)
            {
                header('Location:datamapel.php');
                exit(0);
            }
            else
            {
                header('Location:editmapel.php?nama_mapel=' . $nama_mapel );
                exit(0);
            } 
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function ShowDataKelas($kelas1)
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas="SELECT * FROM kelas WHERE kelas='" . $kelas1 ."'";
    $stmt=$db->prepare($kelas);
       
    $stmt->execute();


    return $stmt->fetch(); 

    }
    public function ShowDataKelas1($kelas1,$nama_jurusan)
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas_sma="SELECT * FROM siswa where (kelas='X' OR kelas='XI' OR kelas='XII')";
    $IPA= "AND (nama_jurusan='IPA' OR nama_jurusan='IPS')";
    $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
    $kelasx= "AND kelas='".$kelas1."'";

    $jurusankelas="";
   
    if($nama_jurusan != ''){
       $jurusankelas = $kelas_sma . $jurusan; 
      
    }if($kelas1 != ''){
        $jurusankelas = $jurusankelas . $kelasx ;
    }elseif($kelas1 != '') {
        $jurusankelas = $kelas_sma . $IPA;
         
    }elseif($nama_jurusan != '') {
        $jurusankelas = $jurusankelas ;
    }else{
       $jurusankelas = $kelas_sma ;
    }
    $stmt=$db->prepare($jurusankelas);
       
    $stmt->execute();


    return $stmt->fetch(); 

    }
    public function ShowDataSemuaKelasGuru($kelas1,$nama_jurusan)
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas_sma="SELECT * FROM siswa where (kelas='VII' OR kelas='VIII' OR kelas='IX' OR kelas='X' OR kelas='XI' OR kelas='XII')";
    $IPA= "AND (nama_jurusan='-' OR nama_jurusan='IPA' OR nama_jurusan='IPS')";
    $jurusan= "AND nama_jurusan='".$nama_jurusan."'";
    $kelasx= "AND kelas='".$kelas1."'";

    $jurusankelas="";
   
    if($nama_jurusan != ''){
       $jurusankelas = $kelas_sma . $jurusan; 
      
    }if($kelas1 != ''){
        $jurusankelas = $jurusankelas . $kelasx ;
    }elseif($kelas1 != '') {
        $jurusankelas = $kelas_sma . $IPA;
         
    }elseif($nama_jurusan != '') {
        $jurusankelas = $jurusankelas ;
    }else{
       $jurusankelas = $kelas_sma ;
    }
    $stmt=$db->prepare($jurusankelas);
       
    $stmt->execute();


    return $stmt->fetch(); 

    }
    public function ShowDataKelasMaster()
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas="SELECT * FROM kelas WHERE (kelas='semua kelas' OR kelas='VII' OR kelas='VIII' OR kelas='IX' ) ";
    $stmt=$db->prepare($kelas);
    $stmt->execute();
    return $stmt;
    }
    public function ShowDataKelasMastersemua()
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas="SELECT * FROM kelas WHERE (kelas='VII' OR kelas='VIII' OR kelas='IX' OR kelas='X' OR kelas='XI' OR kelas='XII' ) ";
    $stmt=$db->prepare($kelas);
    $stmt->execute();
    return $stmt;
    }
    public function ShowDataKelasMastersma()
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas="SELECT * FROM kelas WHERE (kelas='X' OR kelas='XI' OR kelas='XII' ) ";
    $stmt=$db->prepare($kelas);
    $stmt->execute();
    return $stmt;
    }
    public function ShowDataKelasMaster1($halaman_awal,$batas)
    {
        $conn = new koneksi();
    $db=$conn->metal();
    $kelas="SELECT * FROM kelas";
    $stmt=$db->prepare($kelas." LIMIT $halaman_awal , $batas");
    $stmt->execute();
    return $stmt;
    }
    public function insertDataKelasMaster($kelas)
    {
        if (isset($_POST['input']))
        {
            $conn = new koneksi();
            $db=$conn->metal();
            $sql = "INSERT INTO kelas (kelas) VALUES (:kelas)";
            $stmt=$db->prepare($sql);
            $stmt->bindParam(":kelas", $kelas);
            $stmt->execute();
            return true;
            header("location:datakelas.php");

        }
    }
    public function DeleteKelasMaster($kelas)
    {
        $conn = new koneksi();
        $db=$conn->metal();
        $sql="DELETE FROM kelas WHERE kelas = :kelas";
        $stmt=$db->prepare($sql);
        $stmt->bindparam(":kelas", $kelas);
        $stmt->execute();
        return true;
        header("location:datakelas.php");
    }
    public function editDataKelasMaster($kelas,$id_kelas)
    {
     
       
        try{
            $conn = new koneksi();
            $db=$conn->metal();
            $query = "UPDATE kelas SET kelas=:kelas WHERE id_kelas = :id_kelas";
            $stmt = $db->prepare($query);
            $data = [
                ':kelas' => $kelas,
                ':id_kelas'=> $id_kelas
            ];
            
            if (!$stmt->execute($data)) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
                die();
            }
            header('location:datakelas.php?kelas=' . $kelas);
                exit(0);
        
            
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


}

