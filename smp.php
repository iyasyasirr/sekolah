<?php 

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}


require 'user.php';
require 'koneksi.php';
use User\User;
$obj = new User();
?>


<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<body>
    <header>
        <div class="w3-top">

            <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
                <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Kembali</a>
                <form action="input_siswa.php" method="POST" name="login">
                    <input type="submit" class="w3-bar-item1 w3-button w3-padding-large" name="submit"
                        value="input data siswa">
                </form>
    </header>
    </div>
    <br>
    <br>
    <h1>SISWA SMP HARAPAN BANGSA</h1>
    <center>
        <?php 
		$select="";
        $search="";
        if (isset($_POST['search'])) {
            $select = $_POST['select'];
            $search = $_POST['search'];
            $nama_kelas = $_POST['nama_kelas'];
        }
	?>

        <form action="" method="GET">
            <select id="select" name="kelas" size="1">
                <div class="styled">
                <option value="" disabled selected hidden>Filter Kelas</option>
                <option value="VII">VII</option>
                <option value="VIII">VIII</option>
                <option value="IX">IX</option>
              
            </select>
            </div>
            <input type="text" onkeyup="myFunction()" placeholder="search...." name="search" id="search" class="form">
            <button id="submit" name="submit" class="btn btn-warning">Cari</button>
            <a href="smp.php">refresh</a>
            <?php
        $kelas1 = isset($_GET['kelas'])?$_GET['kelas'] : "";
    
        $data=$obj->showDataKelas($kelas1);

            ?>
           
          <h1 style="color: white;">Kelas <?php echo $data != false ? $data['kelas'] : "";?></h1>
     
          </form>
 <?php

    ?>
  
        <h3 style="color: white;">WALIKELAS</h3>
        <table class='table table-bordered table-responsive'id="myTable1">
            <div class="container">
            <tr>
          <th>NO</th>
          <th>NIG</th>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Tingkatan</th>
          <th>Tgl_Lahir</th>
          <th>JK</th>
          <th>Alamat</th>
          <th>Mapel</th>
          </tr>
        </div>
    </center>

    <?php 
    
    $kelas_smp = isset($_GET['kelas'])?$_GET['kelas'] : "";
    
$data=$obj->showDataGuruvii($kelas_smp);
$no=1;
while($row=$data->fetch(PDO::FETCH_ASSOC)){
?>
    <tr>
    <td><?php echo $no++; ?></td>
      <td><?php echo $row['NIG']; ?></td>
      <td><?php echo $row['nama']; ?></td>
      <td><?php echo $row['kelas']; ?></td>
      <td><?php echo $row['Tingkatan']; ?></td>
      <td><?php echo $row['tgl_lahir']; ?></td>
      <td><?php echo $row['jk']; ?></td>
      <td><?php echo $row['alamat']; ?></td>
      <td><?php echo $row['mapel']; ?></td>
    </tr>
    </form>
    <?php 
    }
?>
</table>
<thead>
    <table class='table table-bordered table-responsive'id="myTable">
        <h3 style="color: white;">DATA SISWA</h3>
        <div class="container">
            <tr>
                <th>NO</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Tgl_Lahir</th>
                <th>JK</th>
                <th>Alamat</th>
                <th colspan="5">aksi</th>
            </tr>
        </div>
        </thead>
        </center>

        <?php 
        

       
        $kelas = isset($_GET['kelas'])?$_GET['kelas'] : "";
        
$batas = 5;
$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;
$previous = $halaman - 1;
$next = $halaman + 1;
$data=$obj->showDataSmp($kelas_smp);
$jumlah_data = $data->rowcount();
$total_halaman = ceil($jumlah_data / $batas);
$data_smp=$obj->showDataSmp1($kelas_smp,$batas,$halaman_awal);
$nomor = $halaman_awal+1;
$no=1;
while($row=$data_smp->fetch(PDO::FETCH_ASSOC)){
?>
<tbody>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $row['NIS']; ?></td>
            <td><?php echo $row['nama']; ?></td>
            <td><?php echo $row['kelas']; ?></td>
            <td><?php echo $row['tgl_lahir']; ?></td>
            <td><?php echo $row['jk']; ?></td>
            <td><?php echo $row['alamat']; ?></td>
            <td><a href="editsmp.php?NIS=<?php echo $row['NIS']; ?> " class="btn btn-primary" >Edit</a></td>
            <td><a href="prosesdelete.php?NIS=<?php echo $row['NIS']; ?>" class="btn btn-danger">Hapus</a></td>
        </tr>

        <?php 
    }


?>
</tbody>
</form>
</table>
<nav>
			<ul class="pagination justify-content-center">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ echo "href='?halaman=$previous&select=$select&search=$search'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_halaman;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="?halaman=<?php echo $x ?>&select=<?php echo $select; ?>&search=<?php echo $search ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?halaman=$next&select=$select&search=$search'"; } ?>>Next</a>

				</li>
			</ul>
		</nav>

        <script> 
function myFunction() { 
  // Deklarasi variable 
  var input, filter, table, tr, td, i, a, txtValue; 
  input = document.getElementById("search"); 
  filter = input.value.toUpperCase(); 
  table = document.getElementById("myTable","myTable1");
  tr = table.getElementsByTagName("tr"); 
 
  // Ulangi(looping) melalui semua baris tabel, dan sembunyikan mereka yang tidak cocok dengan kueri pencarian 
  for (i = 0; i < tr.length; i++) { 
    td = tr[i].getElementsByTagName("td");
    for(a = 0; a < td.length; a++){
      if (td[a]) { 
      txtValue = td[a].textContent || td[a].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) { 
        tr[i].style.display = ""; 
        break;
      } else { 
        tr[i].style.display = "none"; 
      } 
    } 
    } 
  } 
} 
</script>
