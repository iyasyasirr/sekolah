<?php
session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}
require 'user.php';
use User\User;

$obj = new User();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>DATA SEKOLAH HARAPAN BANGSA</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<!-- Navbar -->
<div class="w3-top">
    
  <div class="w3-bar w3-teal w3-card w3-left-align w3-large ">
    <div class="navbar">
    <a href ="smp.php" class="w3-bar-item w3-button w3-padding-large">Data SMP</a>
    <a href ="sma.php" class="w3-bar-item w3-button w3-padding-large">Data SMA</a>
    <a href ="guru.php" class="w3-bar-item w3-button w3-padding-large">Data GURU</a>
<a href ="datamapel.php" class="w3-bar-item w3-button w3-padding-large">Data Mapel</a>

</div>
          
     
<a href ="logout.php" class="w3-bar-item1 w3-button w3-padding-large">LogOut</a>
</div>
        </div> 
      </div> 
  </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:500px 16px">
<img src=img/pdibiru.png>
  <h1 class="w3-margin w3-jumbo">SELAMAT DATANG DI HALAMAN ADMIN</h1>
  <p class="w3-xlarge">Silahkan click tombol diatas untuk penginputan data</p>
</header>


</body>
</html>
