<?php
session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}


require 'user.php';
require 'koneksi.php';
use User\User;

$obj = new User();
?>

<head>
  <title>DATA GURU</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/tabel.css">
  <link rel="stylesheet" href="css/nihh.css">
  <link rel="stylesheet" href="css/smp.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
</head>

<body>
  <header>
    <div class="w3-top">
      <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
          <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Kembali</a>
          <form action="input_guru.php" method="POST" name="login">
            <input type="submit" class="w3-bar-item1 w3-button w3-padding-large" name="submit"
              value="Input Data Guru">
          </form>
      </div>
  </header>
  <br>
  <br>
  <br>
  <h1>Guru SMA dan SMP Harapan Bangsa</h1>
  <p>
    <center>
    <?php 
		$select="";
        $search="";
        if (isset($_POST['search'])) {
          $select = $_POST['select'];
          $search = $_POST['search'];
        }
	?>
      <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="GET">
            <select name="id_kelas">
                <option disabled selected hidden>pilih kelas</option>
                <option value="VII">VII</option>
                <option value="VIII">VIII</option>
                <option value="IX">IX</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
            </select>
            <select name="select">
                <option disabled selected hidden>pilih jurusan</option>
                <option value="-">-</option>
                <option value="IPA">IPA</option>
                <option value="IPS">IPS</option>
                </select>
                <input type="text" onkeyup="myFunction()" placeholder="search...." name="search" id="search" class="form">
            <button id="submit" name="submit" class="btn btn-warning">Cari</button>
            
        </form>
        <?php
        $kelas1 = isset($_GET['kelas'])?$_GET['kelas'] : "";
        $nama_jurusan = isset($_GET['select'])?$_GET['select'] : "";
        $data=$obj->showDataSemuaKelasGuru($kelas1,$nama_jurusan);

            ?>
           
      
      <h1 style="color: white;">Guru</h1> <h1 style="color: white;">Kelas <?php echo $data != false ? $data['kelas'] : "";?> Jurusan <?php echo $data != false ? $data['nama_jurusan'] : "";?></h1></h1>
      <table class='table table-bordered table-responsive' id="myTable">
        <div class="container" >
        <tr>
          <th>NO</th>
          <th>NIG</th>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Tingkatan</th>
          <th>Tgl_Lahir</th>
          <th>JK</th>
          <th>Alamat</th>
          <th>Jurusan</th>
          <th>Mapel</th>
          <th colspan="5">aksi</th>
          </thead>
    </center>
    <?php 
     $nama_jurusan = isset($_GET['select'])?$_GET['select'] : "";
     $kelas = isset($_GET['id_kelas'])?$_GET['id_kelas'] : "";
     
$batas = 10;
$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;
$previous = $halaman - 1;
$next = $halaman + 1;
$data=$obj->showDataGuru($nama_jurusan,$kelas);
$jumlah_data = $data->rowcount();
$total_halaman = ceil($jumlah_data / $batas);
$data_guru=$obj->showDataGuru1($nama_jurusan,$kelas,$halaman_awal,$batas);
$nomor = $halaman_awal+1;
$no=1;
	while($row=$data_guru->fetch(PDO::FETCH_ASSOC)){
?>
<tbody>
    <tr>
      <td><?php echo $no++; ?></td>
      <td><?php echo $row['NIG']; ?></td>
      <td><?php echo $row['nama']; ?></td>
      <td><?php echo $row['kelas']; ?></td>
      <td><?php echo $row['Tingkatan']; ?></td>
      <td><?php echo $row['tgl_lahir']; ?></td>
      <td><?php echo $row['jk']; ?></td>
      <td><?php echo $row['alamat']; ?></td>
      <td><?php echo $row['nama_jurusan']; ?></td>
      <td><?php echo $row['mapel']; ?></td>

      <td><a href="editguru.php?NIG=<?php echo $row['NIG']; ?>"class = "btn btn-primary ">Edit</a></td>
      <td><a href="proses_delete_guru.php?NIG=<?php echo $row['NIG']; ?>"class = "btn btn-danger ">Hapus</a></td>
    </tr>
    </form>
    <?php 
 } 

?>
</tbody>
</table>
</body>
<nav>
<ul class="pagination justify-content-center">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ echo "href='?halaman=$previous&search=$search'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_halaman;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="?halaman=<?php echo $x ?>=<?php echo $search ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?halaman=$next&search=$search'"; } ?>>Next</a>

				</li>
			</ul>
		</nav>

<script> 
function myFunction() { 
  // Deklarasi variable 
  var input, filter, table, tr, td, i, a, txtValue; 
  input = document.getElementById("search"); 
  filter = input.value.toUpperCase(); 
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr"); 
 
  // Ulangi(looping) melalui semua baris tabel, dan sembunyikan mereka yang tidak cocok dengan kueri pencarian 
  for (i = 0; i < tr.length; i++) { 
    td = tr[i].getElementsByTagName("td");
    for(a = 0; a < td.length; a++){
      if (td[a]) { 
      txtValue = td[a].textContent || td[a].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) { 
        tr[i].style.display = ""; 
        break;
      } else { 
        tr[i].style.display = "none"; 
      } 
    } 
    } 
  } 
} 
</script>