<?php

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

require 'user.php';
require 'koneksi.php';

use User\User;

$obj = new User();
?>
<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<body>
    <header>
        <div class="w3-top">

            <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
                <a href="index.php" class="w3-bar-item w3-button w3-padding-large">Home</a>
                <form action="input_kelas.php" method="POST" name="login">
                    <input type="submit" class="w3-bar-item1 w3-button w3-padding-large" name="submit"
                        value="Input Data Kelas">

            </div>
    </header>
    <br>
    <br>
    <br>
    <center>
        <h1>DATA SEMUA KELAS HARAPAN BANGSA</h1>
        <?php
          $search="";
          if (isset($_POST['search'])) {
              $search = $_POST['search'];
          }
        ?>
       
        
        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="GET">
        <tr>
            <th><input type="text" onkeyup="myFunction()" placeholder="search...." name="search" id="search" class="form">
            <button id="submit" name="submit" class="btn btn-warning">Cari</button></th>
        </form>

        <table class='table table-bordered table-responsive' id="myTable">
            <div class="">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Mapel</th>
                        <th colspan="5">aksi</th>
                    </tr>
                </thead>
    </center>
    <?php 
    $batas = 5;
    $halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
    $halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;
    $previous = $halaman - 1;
    $next = $halaman + 1;
    $data=$obj->showDataKelasMaster();
    $jumlah_data = $data->rowcount();
    $total_halaman = ceil($jumlah_data / $batas);
	$data_1=$obj->showDataKelasMaster1($halaman_awal,$batas);
    $nomor = $halaman_awal+1;
    $no=1;
	while($row=$data_1->fetch(PDO::FETCH_ASSOC)){
?>
    <tbody>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $row['kelas']; ?></td>
            <td><a href="editkelas.php?kelas=<?php echo $row['kelas']; ?>" class="btn btn-primary ">Edit</a></td>
            <td><a href="proses_delete_kelas.php?kelas=<?php echo $row['kelas']; ?>"
                    class="btn btn-danger ">Hapus</a></td>
        </tr>
    <?php 
 } 
 ?>
    </tbody>
    </form>
    </table>
 
    <nav>
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link"
                    <?php if($halaman > 1){ echo "href='?halaman=$previous&search=$search'"; } ?>>Previous</a>
            </li>
            <?php 
				for($x=1;$x<=$total_halaman;$x++){
					?>
            <li class="page-item"><a class="page-link"
                    href="?halaman=<?php echo $x ?>>&search=<?php echo $search ?>"><?php echo $x; ?></a>
            </li>
            <?php
				}
				?>
            <li class="page-item">
                <a class="page-link"
                    <?php if($halaman < $total_halaman) { echo "href='?halaman=$next&search=$search'"; } ?>>Next</a>

            </li>
        </ul>
    </nav>

    <script>
        function myFunction() {
            // Deklarasi variable 
            var input, filter, table, tr, td, i, a, txtValue;
            input = document.getElementById("search");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Ulangi(looping) melalui semua baris tabel, dan sembunyikan mereka yang tidak cocok dengan kueri pencarian 
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td");
                for (a = 0; a < td.length; a++) {
                    if (td[a]) {
                        txtValue = td[a].textContent || td[a].innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                            break;
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        }
    </script>