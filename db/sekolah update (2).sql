-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2022 at 11:51 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `NIG` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(20) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `Tingkatan` varchar(10) NOT NULL,
  `nama_jurusan` varchar(50) DEFAULT NULL,
  `mapel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`NIG`, `nama`, `tgl_lahir`, `jk`, `alamat`, `kelas`, `Tingkatan`, `nama_jurusan`, `mapel`) VALUES
(1231241, 'arif', '2022-10-03', 'laki-laki', 'bandung', 'XI', 'SMA', 'IPS', 'matematika'),
(1267853, 'RUDI', '2022-10-11', 'laki-laki', 'bandung', 'X', 'SMA', 'IPS', 'IPA'),
(12124123, 'daniel', '2022-10-23', 'laki-laki', 'badnug', 'VII', 'SMP', '-', 'IPS'),
(12412341, 'Sanny', '2022-10-19', 'laki-laki', 'bas', 'VIII', 'SMP', '-', 'IPS'),
(12418247, 'jijij', '2022-09-01', 'laki-laki', 'bandung', '1', 'SMP', 'TIDAK ADA', 'IPA'),
(12847912, 'asep', '2022-10-04', 'laki-laki', 'bandung', 'IX', 'SMP', 'TIDAK ADA', 'PJOK'),
(17236457, 'neni', '2022-10-10', 'laki-laki', 'jakarta', 'XI', 'SMA', 'IPA', 'IPS'),
(123124124, 'agus', '2022-10-19', 'laki-laki', 'asdasd', 'VII', 'SMP', 'TIDAK ADA', 'IPS'),
(127634517, 'sergio', '2022-10-24', 'laki-laki', 'bandung', 'IX', 'SMP', '-', 'pai'),
(132112412, 'yasir', '2022-09-20', 'laki-laki', 'bandung', '4', 'SMA', 'IPA', 'basis data'),
(172345765, 'jordy', '2022-10-17', 'laki-laki', 'bandung', 'XII', 'SMA', 'IPA', 'pai'),
(712634567, 'jessica', '2022-10-12', 'perempuan', 'jakarta', 'VIII', 'SMP', '-', 'IPA'),
(1231241212, 'rini', '2022-10-17', 'laki-laki', 'bandung', 'XII', 'SMA', 'IPS', 'matematika'),
(2147483647, 'JOJI', '2022-10-02', 'laki-laki', 'bandung', 'X', 'SMA', 'IPA', 'matematika');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `kode_jurusan` varchar(10) NOT NULL,
  `nama_jurusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`kode_jurusan`, `nama_jurusan`) VALUES
('-', ''),
('IPA', 'ilmu pengetahuan ala'),
('IPS', 'ilmu pengetahuan sos');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`) VALUES
(1, 'IX'),
(2, 'VII'),
(3, 'VIII'),
(4, 'X'),
(5, 'XI'),
(6, 'XII');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`) VALUES
(1, 'basis data'),
(2, 'IPA'),
(3, 'IPS'),
(4, 'matematika'),
(5, 'pai'),
(6, 'PJOK'),
(7, 'pwpb');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `NIS` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `tingkatan` varchar(15) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(20) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `nama_jurusan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`NIS`, `nama`, `kelas`, `tingkatan`, `tgl_lahir`, `jk`, `alamat`, `nama_jurusan`) VALUES
(1247567, 'basd', 'VIII', 'SMA', '2022-09-27', 'laki-laki', 'bandung', 'IPS'),
(1253461, 'yunus', 'X', 'SMA', '2022-10-03', 'laki-laki', 'bandung', 'IPA'),
(1294762, 'dika', 'XI', 'SMP', '2022-10-03', 'laki-laki', 'bandung', 'IPS'),
(1726537, 'febi', 'XI', 'SMA', '2022-10-11', 'laki-laki', 'bandung', 'IPS'),
(1762537, 'kevin', 'X', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPA'),
(1762538, 'zainal', 'X', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPA'),
(1827346, 'angga', 'VII', 'SMP', '2022-11-01', 'laki-laki', 'bandung', 'TIDAK ADA'),
(6523543, 'zaki', 'XII', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPA'),
(7612576, 'chuka', 'XII', 'SMA', '2022-10-24', 'laki-laki', 'bandung', 'IPS'),
(7651276, 'panji', 'XII', 'SMA', '2022-10-18', 'laki-laki', 'bandung', 'IPS'),
(8716287, 'gerry', 'XII', 'SMA', '2022-10-09', 'laki-laki', 'bandung', 'IPS'),
(12315264, 'akmal', 'VIII', 'SMP', '2022-09-26', 'laki-laki', 'bandung', 'TIDAK ADA'),
(12341421, 'abdan', 'XII', 'SMA', '2022-10-18', 'laki-laki', 'bandung', 'IPA'),
(12367576, 'fikri', 'XII', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPS'),
(12736574, 'felissa', 'IX', 'SMP', '2022-10-25', 'perempuan', 'bandung', 'TIDAK ADA'),
(12763512, 'messi', 'VIII', 'SMP', '2022-10-03', 'laki-laki', 'bandung', 'TIDAK ADA'),
(12873642, 'hadi', 'XI', 'SMA', '2022-10-14', 'laki-laki', 'bandung', 'IPS'),
(12873668, 'febi', 'XII', 'SMA', '2022-10-17', 'laki-laki', 'bandung', 'IPS'),
(12873684, 'azky', 'XI', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPS'),
(17265376, 'mail', 'XI', 'SMA', '2022-10-05', 'laki-laki', 'bandung', 'IPA'),
(65426436, 'alya', 'XII', 'SMA', '2022-09-27', 'laki-laki', 'bandung', 'IPA'),
(81267487, 'alfan', 'XI', 'SMA', '2022-10-12', 'laki-laki', 'bandung', 'IPS'),
(81726348, 'fajar', 'XI', 'SMA', '2022-10-04', 'laki-laki', 'bandung', 'IPS'),
(123124561, 'tiara', 'IX', 'SMP', '2022-10-10', 'perempuan', 'bandung', 'TIDAK ADA'),
(123461241, 'taufik', 'VII', 'SMP', '2022-10-03', 'laki-laki', 'bali', 'TIDAK ADA'),
(126735712, 'jajang', 'VIII', 'SMP', '2022-10-10', 'laki-laki', 'jakarta', 'TIDAK ADA'),
(126738127, 'revi', 'VIII', 'SMP', '2022-10-04', 'perempuan', 'bandung', 'TIDAK ADA'),
(126784518, 'iman', 'X', 'SMA', '2022-10-11', 'laki-laki', 'bandung', 'IPS'),
(127635712, 'jufri', 'X', 'SMA', '2022-10-03', 'laki-laki', 'bandung', 'IPS'),
(127836412, 'alvi', 'XI', 'SMA', '2022-10-24', 'laki-laki', 'bandung', 'IPS'),
(128381246, 'risqi', 'VII', 'SMP', '2022-10-17', 'laki-laki', 'bandung', 'TIDAK ADA'),
(172635476, 'sani', 'X', 'SMA', '2022-10-05', 'laki-laki', 'bandung', 'IPA'),
(172635712, 'rizal', 'VII', 'SMP', '2022-10-18', 'laki-laki', 'bandung', 'TIDAK ADA'),
(172635765, 'azhar', 'XI', 'SMA', '2022-10-10', 'laki-laki', 'bandung', 'IPA'),
(172635786, 'seka', 'IX', 'SMP', '2022-10-10', 'laki-laki', 'bandung', 'TIDAK ADA'),
(176253786, 'udin', 'IX', 'SMP', '2022-10-04', 'laki-laki', 'bandung', 'TIDAK ADA'),
(182467321, 'meyla', 'XII', 'SMA', '2022-10-24', 'perempuan', 'bandung', 'IPA'),
(182734612, 'yusuf', 'XI', 'SMA', '2022-10-03', 'laki-laki', 'bandung', 'IPS'),
(182736812, 'reffa', 'XI', 'SMA', '2022-10-12', 'laki-laki', 'bandung', 'IPS'),
(187641234, 'fajar', 'IX', 'SMA', '2022-10-11', 'laki-laki', 'bandung', 'IPA'),
(712654712, 'ujang', 'VII', 'SMP', '2022-10-10', 'laki-laki', 'jakarta', 'TIDAK ADA'),
(716253765, 'rendi', 'X', 'SMA', '2022-10-17', 'laki-laki', 'jogja', 'IPS'),
(781628123, 'rafiki', 'X', 'SMP', '2022-10-04', 'laki-laki', 'bandung', 'IPS'),
(812763812, 'faris', 'XI', 'SMA', '2022-10-11', 'laki-laki', 'bandung', 'IPA'),
(1235465121, 'aceng', 'IX', 'SMP', '2022-10-18', 'laki-laki', 'bandung', 'TIDAK ADA'),
(1278934123, 'yasirr', 'X', 'SMA', '2022-10-03', 'laki-laki', 'bandung', 'IPA'),
(1726357123, 'amel', 'X', 'SMA', '2022-10-11', 'laki-laki', 'bandung', 'IPS'),
(1824768124, 'zidan', 'XI', 'SMA', '2022-10-17', 'laki-laki', 'bandung', 'IPA'),
(2147483647, 'amay', 'XI', 'SMA', '2022-10-09', 'laki-laki', 'bandung', 'IPA');

-- --------------------------------------------------------

--
-- Table structure for table `tingkatan`
--

CREATE TABLE `tingkatan` (
  `Tingkatan` varchar(20) NOT NULL,
  `nama_tingkatan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tingkatan`
--

INSERT INTO `tingkatan` (`Tingkatan`, `nama_tingkatan`) VALUES
('SMA', ''),
('SMP', ''),
('TIDAK ADA', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`) VALUES
('angga', 'angga'),
('risqi', 'risqi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`NIG`),
  ADD KEY `kelas` (`kelas`),
  ADD KEY `mapel` (`mapel`),
  ADD KEY `nama_jurusan` (`nama_jurusan`),
  ADD KEY `Tingkatan` (`Tingkatan`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`kode_jurusan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`NIS`),
  ADD KEY `nama_jurusan` (`nama_jurusan`),
  ADD KEY `kelas` (`kelas`),
  ADD KEY `tingkatan` (`tingkatan`);

--
-- Indexes for table `tingkatan`
--
ALTER TABLE `tingkatan`
  ADD PRIMARY KEY (`Tingkatan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `NIS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2147483648;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
