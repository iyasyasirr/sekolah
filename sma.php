<?php

session_start();
if (!isset($_SESSION['username'])){
    header("Location: login.php");
}

require 'user.php';
require 'koneksi.php';
use User\User;

$obj = new User();
?>

<link rel="stylesheet" type="text/css" href="css/tabel.css">
<link rel="stylesheet" href="css/nihh.css">
<link rel="stylesheet" href="css/smp.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- jQuery -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>


<body>
    <header>
        <div class="w3-top">
            <div class="w3-bar w3-teal1 w3-card w3-left-align w3-large ">
                <a href="index.php" class="w3-bar-item w3-button w3-padding-large ">Kembali</a>
                <form action="input_siswa.php" method="POST" name="login">
                    <input type="submit" class="w3-bar-item1 w3-button w3-padding-large" name="submit"
                        value="input data siswa">
                </form>
            </div>
    </header>
    <br>
    <br>
    <br>
    <h1>SISWA SMA HARAPAN BANGSA</h1>
    <center>
        <?php 
		$select="";
        $search="";
        if (isset($_POST['search'])) {
            $select = $_POST['select'];
            $search = $_POST['search'];
        }
	?>

        <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="GET">
        <select name="kelas">
                <option disabled selected hidden>pilih kelas</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
            </select>
            </select>
            </select>
            <select name="select">
                <option disabled selected hidden>pilih jurusan</option>
                <option value="IPA">IPA</option>
                <option value="IPS">IPS</option>
            </select>
            <input type="text" onkeyup="myFunction()" placeholder="search...." name="search" id="search" class="form">
            <button id="submit" name="submit" class="btn btn-warning">Cari</button>
        </form>
        <?php
        $kelas1 = isset($_GET['kelas'])?$_GET['kelas'] : "";
        $nama_jurusan = isset($_GET['select'])?$_GET['select'] : "";
        $data=$obj->showDataKelas1($kelas1,$nama_jurusan);

            ?>
           
          <h1 style="color: white;">Kelas <?php echo $data != false ? $data['kelas'] : "";?> Jurusan <?php echo $data != false ? $data['nama_jurusan'] : "";?></h1></h1>
     
<h3 style="color: white;">Wali kelas</h3>
        <table class='table table-bordered table-responsive'>
            <div class="container">

                <tr>
                    <th>NO</th>
                    <th>NIG</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Tgl_Lahir</th>
                    <th>JK</th>
                    <th>Alamat</th>
                    <th>nama_jurusan</th>
                </tr>
            </div>
    </center>
    <?php

    $nama_jurusan = isset($_GET['select'])?$_GET['select'] : "";
    $kelas = isset($_GET['kelas'])?$_GET['kelas'] : "";
  

$data=$obj->showDataGuruJurusanSma($nama_jurusan,$kelas);
$no=1;
  while($row=$data->fetch(PDO::FETCH_ASSOC)){ 
 ?>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $row['NIG']; ?></td>
        <td><?php echo $row['nama']; ?></td>
        <td><?php echo $row['kelas']; ?></td>
        <td><?php echo $row['tgl_lahir']; ?></td>
        <td><?php echo $row['jk']; ?></td>
        <td><?php echo $row['alamat']; ?></td>
        <td><?php echo $row['nama_jurusan']; ?></td>
    </tr>

    </form>
    <?php
}



?>

<table class='table table-bordered table-responsive' id="myTable" >
    <thead>
    <h3 style="color: white;">Data Siswa</h3>
            <div class="container">
                <tr>
                    <th>NO</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Tgl_Lahir</th>
                    <th>JK</th>
                    <th>Alamat</th>
                    <th>nama_jurusan</th>
                    <th colspan="5">aksi</th>
                </tr>
</div>
</thead>

    </center>
    <?php
    $nama_jurusan = isset($_GET['select'])?$_GET['select'] : "";
    $kelas = isset($_GET['kelas'])?$_GET['kelas'] : "";

$batas = 5;
$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;
$previous = $halaman - 1;
$next = $halaman + 1;
$data=$obj->showDataJurusanSma($nama_jurusan,$kelas);
$jumlah_data = $data->rowcount();
$total_halaman = ceil($jumlah_data / $batas);
$data_sma=$obj->showDataJurusanSma1($nama_jurusan,$kelas,$halaman_awal,$batas);
$nomor = $halaman_awal+1;
$no=1;
  while($row=$data_sma->fetch(PDO::FETCH_ASSOC)){ 
 ?>
 <tbody>
    <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $row['NIS']; ?></td>
        <td><?php echo $row['nama']; ?></td>
        <td><?php echo $row['kelas']; ?></td>
        <td><?php echo $row['tgl_lahir']; ?></td>
        <td><?php echo $row['jk']; ?></td>
        <td><?php echo $row['alamat']; ?></td>
        <td><?php echo $row['nama_jurusan']; ?></td>
        <td><a href="editsma.php?NIS=<?php echo $row['NIS']; ?>"class="btn btn-primary ">Edit</a></td>
        <td><a href="proses_delete_sma.php?NIS=<?php echo $row['NIS']; ?>"class="btn btn-danger ">Hapus</a></td>
    </tr>
<?php
}
?>
 </form>
    </tbody>
</table>
<nav>
			<ul class="pagination justify-content-center">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ echo "href='?halaman=$previous&select=$select&search=$search'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_halaman;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="?halaman=<?php echo $x ?>&select=<?php echo $select; ?>&search=<?php echo $search ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?halaman=$next&select=$select&search=$search'"; } ?>>Next</a>

				</li>
			</ul>
		</nav>

<script> 
function myFunction() { 
  // Deklarasi variable 
  var input, filter, table, tr, td, i, a, txtValue; 
  input = document.getElementById("search"); 
  filter = input.value.toUpperCase(); 
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr"); 
 
  // Ulangi(looping) melalui semua baris tabel, dan sembunyikan mereka yang tidak cocok dengan kueri pencarian 
  for (i = 0; i < tr.length; i++) { 
    td = tr[i].getElementsByTagName("td");
    for(a = 0; a < td.length; a++){
      if (td[a]) { 
      txtValue = td[a].textContent || td[a].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) { 
        tr[i].style.display = ""; 
        break;
      } else { 
        tr[i].style.display = "none"; 
      } 
    } 
    } 
  } 
} 
</script>